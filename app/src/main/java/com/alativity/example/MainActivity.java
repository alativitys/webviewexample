package com.alativity.example;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        web = findViewById(R.id.web_view);

        LoadWebView("file:///android_asset/index.html");
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void LoadWebView(String url) {

        android.webkit.CookieManager cookieManager = android.webkit.CookieManager.getInstance();
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setDomStorageEnabled(true);
        web.setWebViewClient(new WebViewClient());
        cookieManager.setAcceptCookie(true);
        cookieManager.acceptCookie();
        CookieManager.setAcceptFileSchemeCookies(true);
        CookieManager.getInstance().setAcceptCookie(true);
        cookieManager.getCookie(url);
        web.loadUrl(url);
    }
}